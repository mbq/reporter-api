use anyhow::Result;
use bincode::{DefaultOptions, Options};
use log::info;
use rand::distributions::Alphanumeric;
use rand::{thread_rng, Rng};
use serde::{Deserialize, Serialize};
use serde_json::{json, Value};
use std::path::PathBuf;
use std::time::{SystemTime, UNIX_EPOCH};

#[derive(Copy, Clone, Deserialize, Serialize, PartialEq)]
struct Revision(u128);

impl Revision {
    fn new() -> Revision {
        let ans: u128 = SystemTime::now()
            .duration_since(UNIX_EPOCH)
            .unwrap()
            .as_millis();
        let modif = rand::thread_rng().gen::<u16>();
        Revision(ans * 65536 + u128::from(modif))
    }
    fn root() -> Revision {
        Revision(0)
    }
    fn from_bytes(bytes: &[u8]) -> Revision {
        DefaultOptions::new()
            .with_big_endian()
            .deserialize(bytes)
            .unwrap()
    }
    fn serialize(&self) -> Vec<u8> {
        DefaultOptions::new()
            .with_big_endian()
            .serialize(&self)
            .unwrap()
    }
    fn from_str(x: &str) -> Result<Revision> {
        Ok(Revision(u128::from_str_radix(x, 36)?))
    }
}
use std::fmt;
impl fmt::Display for Revision {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}", radix_fmt::radix_36(self.0))
    }
}

#[derive(Clone, Deserialize, Serialize)]
struct Entry {
    parents: Vec<Revision>,
    id: String,
    payload: String,
}
impl Entry {
    fn from_bytes(bytes: &[u8]) -> Option<Entry> {
        bincode::deserialize(bytes).ok()
    }
    fn serialize(&self) -> Vec<u8> {
        bincode::serialize(&self).unwrap()
    }
}

#[derive(Clone, Deserialize, Serialize, PartialEq)]
struct Pass {
    id: String,
    secret: String,
}
impl Pass {
    fn from_bytes(bytes: &[u8]) -> Option<Self> {
        bincode::deserialize(bytes).ok()
    }
    fn serialize(&self) -> Vec<u8> {
        bincode::serialize(&self).unwrap()
    }
}

#[derive(Clone, Deserialize, Serialize)]
struct Expires(u64);

impl Expires {
    fn new() -> Expires {
        Expires(
            SystemTime::now()
                .duration_since(UNIX_EPOCH)
                .unwrap()
                .as_secs()
                + 7 * 24 * 60 * 60,
        )
    }
    fn is_valid(&self) -> bool {
        let cur = SystemTime::now()
            .duration_since(UNIX_EPOCH)
            .unwrap()
            .as_secs();
        cur < self.0
    }
    fn from_bytes(bytes: &[u8]) -> Option<Expires> {
        bincode::deserialize(bytes).ok()
    }
    fn serialize(&self) -> Vec<u8> {
        bincode::serialize(&self).unwrap()
    }
}

#[derive(Clone)]
pub struct Memory {
    db: sled::Db,
}

#[derive(Clone, Deserialize, Serialize)]
struct Login {
    id: String,
    secret: String,
}

fn random_string(len: usize) -> String {
    let mut rng = thread_rng();
    String::from_utf8(
        std::iter::repeat(())
            .map(|_| rng.sample(Alphanumeric))
            .take(len)
            .collect(),
    )
    .unwrap()
}

impl Memory {
    pub fn new(path: Option<&PathBuf>) -> Result<Memory> {
        Ok(Memory {
            db: if let Some(path) = path {
                sled::Config::default().path(path).open()?
            } else {
                sled::Config::default().temporary(true).open()?
            },
        })
    }
    pub fn scan(&self, id: Option<&str>) -> Result<Option<(String, String, Option<String>)>> {
        let heads = self.db.open_tree(b"head")?;
        let data = self.db.open_tree(b"data")?;
        let (cur_id_enc, cur_data) = match id {
            None => {
                if let Some((id_enc, rev_enc)) = heads.first()? {
                    let entry_enc = data.get(rev_enc)?.unwrap();
                    let entry = Entry::from_bytes(&entry_enc).unwrap();
                    (id_enc, entry.payload)
                } else {
                    return Ok(None);
                }
            }
            Some(pfx) => {
                let cur_id_enc = sled::IVec::from(pfx.as_bytes());
                if let Some(rev_enc) = heads.get(&cur_id_enc)? {
                    let entry_enc = data.get(rev_enc)?.unwrap();
                    let entry = Entry::from_bytes(&entry_enc).unwrap();
                    (cur_id_enc, entry.payload)
                } else {
                    return Ok(None);
                }
            }
        };

        use std::str::from_utf8;
        let cur_id = id
            .unwrap_or_else(|| from_utf8(&cur_id_enc).unwrap())
            .to_string();

        if let Some((next_id_enc, _)) = heads.get_gt(cur_id_enc)? {
            let next_id = from_utf8(&next_id_enc)?;
            Ok(Some((cur_data, cur_id, Some(next_id.to_string()))))
        } else {
            Ok(Some((cur_data, cur_id, None)))
        }
    }
    pub fn secrets(&self, id: Option<&str>) -> Result<Option<(String, String, Option<String>)>> {
        let owns = self.db.open_tree(b"owns")?;
        let (cur_id_enc, cur_secret) = match id {
            None => {
                if let Some((id_enc, pass_enc)) = owns.first()? {
                    (id_enc, Pass::from_bytes(&pass_enc).unwrap().secret)
                } else {
                    return Ok(None);
                }
            }
            Some(pfx) => {
                let cur_id_enc = sled::IVec::from(pfx.as_bytes());
                if let Some(pass_enc) = owns.get(&cur_id_enc)? {
                    (cur_id_enc, Pass::from_bytes(&pass_enc).unwrap().secret)
                } else {
                    return Ok(None);
                }
            }
        };

        use std::str::from_utf8;
        let cur_id = id
            .unwrap_or_else(|| from_utf8(&cur_id_enc).unwrap())
            .to_string();

        if let Some((next_id_enc, _)) = owns.get_gt(cur_id_enc)? {
            let next_id = from_utf8(&next_id_enc)?;
            Ok(Some((cur_secret, cur_id, Some(next_id.to_string()))))
        } else {
            Ok(Some((cur_secret, cur_id, None)))
        }
    }
    pub fn save(&self, id: &str, payload: &Value, from: &str) -> Result<(String, Option<String>)> {
        let from_rev = Revision::from_str(from)?;
        let heads = self.db.open_tree(b"head")?;
        let data = self.db.open_tree(b"data")?;
        let old_rev = heads
            .get(id.as_bytes())?
            .map(|x| Revision::from_bytes(&x))
            .unwrap_or_else(Revision::root);
        //TODO: "old" revision can be newer han "from" revision
        //Swap them up if so.
        let merged = if old_rev == from_rev {
            None
        } else {
            use crate::merge::merge;
            info!(
                "Merging commenced {}><{}",
                old_rev.to_string(),
                from_rev.to_string()
            );
            //TODO: Check that old_rev's ID is id; otherwise, data leak.
            let old_entry: Value = data
                .get(old_rev.serialize())?
                .and_then(|x| Entry::from_bytes(&x))
                .and_then(|x| serde_json::from_str(&x.payload).ok())
                .unwrap_or_else(|| json!({})); //Actually panic borked data
            Some(merge(&old_entry, payload))
        };

        //TODO: Do so that saved and returned payloads have all ""s removed

        let entry = Entry {
            parents: if old_rev != from_rev {
                vec![old_rev, from_rev]
            } else {
                vec![from_rev]
            },
            id: id.to_string(),
            payload: merged
                .clone()
                .map(|x| x.to_string())
                .unwrap_or_else(|| payload.to_string()),
        };
        let new_rev = Revision::new();
        data.insert(new_rev.serialize(), entry.serialize())?;
        heads.insert(id.as_bytes(), new_rev.serialize())?;
        Ok((new_rev.to_string(), merged.map(|x| x.to_string())))
    }
    pub fn get(&self, id: &str, vs: Option<String>) -> Result<Option<(String, String)>> {
        let head = self.db.open_tree(b"head")?;
        let data = self.db.open_tree(b"data")?;

        if let Some(rev_enc) = head.get(id.as_bytes())? {
            let revision = Revision::from_bytes(&rev_enc);
            let no_change: bool = match (vs, revision) {
                (None, _) => false,
                (Some(a_enc), b) => match Revision::from_str(&a_enc) {
                    Err(_) => false,
                    Ok(a) => (a == b),
                },
            };

            if no_change {
                //No changes found
                Ok(None)
            } else if let Some(data_enc) = data.get(rev_enc)? {
                if let Some(data) = Entry::from_bytes(&data_enc) {
                    Ok(Some((revision.to_string(), data.payload)))
                } else {
                    //Trouble  -- saved data was borked
                    panic!("Borked data")
                    //  Ok(Some("{}".to_string()))
                }
            } else {
                //TODO: Trouble -- head goes into void
                //No data found
                panic!("Head into void")
                //Err(()))
                //Ok(Some("{}".to_string()))
            }
        } else {
            //There is no such data
            Ok(Some((Revision::root().to_string(), "{}".to_string())))
        }
    }
    pub fn do_authenticate(&self, id: &str, secret: &str) -> Result<bool> {
        let owns = self.db.open_tree(b"owns")?;

        let caller = Pass {
            id: id.to_string(),
            secret: secret.to_string(),
        };
        if let Some(owner_enc) = owns.get(&id.as_bytes())? {
            //That ID is owned
            match Pass::from_bytes(&owner_enc) {
                Some(owner) => Ok(owner == caller),
                None => panic!("Invalid owns record under {}", id),
            }
        } else {
            info!("New record created for {}", id);
            //That ID is not owned
            owns.insert(id.as_bytes(), caller.serialize())?;
            Ok(true)
        }
    }
    pub fn do_authorize(&self, prefix: &str) -> Result<String> {
        let secret = random_string(21);
        let token: String = prefix.to_string() + &secret;
        self.db.open_tree(b"token")?.insert(
            token.as_bytes(),
            bincode::serialize(&Expires::new()).unwrap(),
        )?;
        Ok(secret)
    }
    pub fn transfer(&self, id: &str, new_secret: &str, transfer_token: &str) -> Result<bool> {
        if self
            .db
            .open_tree(b"ttokens")?
            .remove(&transfer_token)?
            .is_some()
        {
            info!(
                "Transferring ownership of {} using valid token {}",
                id, transfer_token
            );
            self.drop_ownership(id)?;
            self.do_authenticate(id, new_secret)
        } else {
            //Wrong token, sorry
            info!("Wrong transfer token used {}", transfer_token);
            Ok(false)
        }
    }
    pub fn add_transfer_token(&self) -> Result<String> {
        let secret = random_string(21);
        info!("Generated new transfer token");
        self.db
            .open_tree(b"ttokens")?
            .insert((&secret).as_bytes(), b"")?;
        Ok(secret)
    }
    pub fn drop_ownership(&self, id: &str) -> Result<bool> {
        info!("Removing ownership of {}", id);
        if let Some(x) = self.db.open_tree(b"owns")?.remove(id)? {
            let past = Pass::from_bytes(&x).unwrap().secret;
            self.db
                .open_tree(b"past")?
                .insert(("".to_string() + id + "::" + &past).as_bytes(), b"")?;
            Ok(true)
        } else {
            Ok(false)
        }
    }
    pub fn deauth(&self, token: &str) -> Result<()> {
        Ok(self
            .db
            .open_tree(b"token")?
            .remove(token.as_bytes())
            .map(|_| ())?)
    }
    pub fn is_authorized(&self, token: &str, bump: bool) -> Result<bool> {
        let token_tree = self.db.open_tree(b"token")?;
        Ok(if let Some(enc) = token_tree.get(token.as_bytes())? {
            //Something found
            if let Some(exp) = Expires::from_bytes(&enc) {
                if exp.is_valid() {
                    //Valid, bump
                    if bump {
                        token_tree.insert(token.as_bytes(), Expires::new().serialize())?;
                    }
                    true
                } else {
                    //Outdated, ditch from db
                    token_tree.remove(token.as_bytes())?;
                    false
                }
            } else {
                //Invalid, ditch from db -- TODO Log this stuff, shouldn't happen
                panic!("Invalid token expiration record")
                //token_tree.remove(token.as_bytes())?;
                //false
            }
        } else {
            //Not present
            false
        })
    }
}
