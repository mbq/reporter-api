use serde_json::Value;

//Based on json_value_merge

fn merge_into(a: &mut Value, b: &Value) {
    match (a, b) {
        (&mut Value::Object(ref mut a), &Value::Object(ref b)) => {
            for (k, v) in b {
                merge_into(a.entry(k.clone()).or_insert(Value::Null), v);
            }
        }
        (&mut Value::Array(ref mut a), &Value::Array(ref b)) => {
            a.extend(b.clone());
            a.dedup();
        }
        (&mut Value::Array(ref mut _a), _b) => {} //Never delete an array
        (a, b) => {
            *a = b.clone();
        }
    }
}

pub fn merge(a: &Value, b: &Value) -> Value {
    let mut new = a.clone();
    merge_into(&mut new, b);
    new
}
