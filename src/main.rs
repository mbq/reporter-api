use futures::join;
use log::info;
use memory::Memory;
use serde::Serialize;
use serde_json::Value;
use std::net::SocketAddr;
use std::path::PathBuf;
use structopt::StructOpt;
use tokio::signal;
use warp::reply::json;
use warp::Filter;

mod memory;
mod merge;

#[derive(Serialize)]
struct JsErr {
    err: String,
}
impl JsErr {
    fn new(err: String) -> Self {
        JsErr { err }
    }
}
#[derive(Serialize)]
struct JsOk {
    ok: String,
}
impl JsOk {
    fn new(pay: String) -> Self {
        JsOk { ok: pay }
    }
}
#[derive(Serialize)]
struct JsPay {
    revision: String,
    token: Option<String>,
    payload: Option<String>,
}

#[derive(Serialize)]
struct JsRecord {
    id: String,
    next_id: Option<String>,
    payload: String,
}

async fn shutdown_signal() {
    signal::ctrl_c()
        .await
        .expect("Ctrl-C handler installation failed.");
}

async fn slowdown<T: warp::Reply>(response: T) -> Result<impl warp::Reply, warp::Rejection> {
    tokio::time::sleep(std::time::Duration::from_millis(1000)).await;
    Ok(response)
}

const HTML_STUFF: &str = r#"<!DOCTYPE html><html><head>
 <meta charset="utf-8"/>
 <meta name="viewport" content="width=device-width,initial-scale=1"/>
 <link rel="icon" type="image/png" href="/dist/icon.png"/>
 </head>
 <body>JavaScript is required.<script src="/dist/main.js"></script></body>
</html>"#;

#[derive(StructOpt, Debug)]
#[structopt(name = "pims-reporter", about = "Reporter API server")]
struct Config {
    #[structopt(short, long, parse(from_os_str))]
    /// Path to DB
    ///
    /// If skipped, temporary, auto-destructible DB is created in SHM and deleted after the program
    /// exit.
    db: Option<PathBuf>,

    #[structopt(short, long, default_value = "127.0.0.1:3030")]
    /// Address to listen to for the main API as well as front-end files
    ///
    /// Here, you welcome ingress from the web.
    listen_address: SocketAddr,

    #[structopt(short, long, default_value = "3131")]
    /// Port to listen to admin commands
    ///
    /// This always binds to loop-back 127.0.0.1 for security.
    admin_port: u16,
}

#[tokio::main]
async fn main() {
    env_logger::init();

    let cfg = Config::from_args();
    info!("Got cl parameters: {:?}", cfg);

    let memory = Memory::new(cfg.db.as_ref()).unwrap();

    //TODO: Statuses
    let gm = memory.clone();
    let getter = warp::path!("api" / String)
        .and(warp::get())
        .and(warp::header::<String>("X-Token"))
        .and(warp::header::optional::<String>("X-Revision"))
        .map(move |id: String, token: String, rev: Option<String>| {
            let server_error = JsErr::new("server error".to_string());
            let disallow = JsErr::new("not allowed".to_string());
            match gm.is_authorized(&(id.clone() + &token), false) {
                Ok(true) => match gm.get(&id, rev) {
                    Ok(Some((revision, payload))) => json(
                        &(JsPay {
                            revision,
                            token: None,
                            payload: Some(payload),
                        }),
                    ),
                    Ok(None) => json(&()),
                    Err(_) => json(&server_error),
                },
                Ok(false) => json(&disallow),
                Err(_) => json(&server_error),
            }
        });

    let gm = memory.clone();
    let authenticator = warp::path!("api" / String)
        .and(warp::post())
        .and(warp::header::<String>("X-Password"))
        .and(warp::header::optional::<String>("X-Transfer-Token"))
        .map(
            move |id: String, password: String, transfer_token: Option<String>| {
                let server_error = JsErr::new("server error".to_string());
                let disallow = JsErr::new("not allowed".to_string());
                let outcome = match transfer_token {
                    Some(tt) => gm.transfer(&id, &password, &tt),
                    None => gm.do_authenticate(&id, &password),
                };
                match outcome {
                    Ok(true) => match gm.do_authorize(&id) {
                        Ok(token) => match gm.get(&id, None) {
                            Ok(None) => panic!("Null revision for null versus"),
                            Ok(Some((revision, payload))) => json(
                                &(JsPay {
                                    revision,
                                    token: Some(token),
                                    payload: Some(payload),
                                }),
                            ),
                            Err(_) => json(&server_error),
                        },
                        Err(_) => json(&server_error),
                    },
                    Ok(false) => json(&disallow),
                    Err(_) => json(&server_error),
                }
            },
        )
        .and_then(slowdown);

    let gm = memory.clone();
    let setter = warp::path!("api" / String)
        .and(warp::post())
        .and(warp::header::<String>("X-Token"))
        .and(warp::header::<String>("X-Revision"))
        .and(warp::body::content_length_limit(32 * 1024 * 1024))
        .and(warp::body::json())
        .map(
            move |id: String, token: String, revision: String, body: Value| {
                let server_error = JsErr::new("server error".to_string());
                let disallow = JsErr::new("not allowed".to_string());
                match gm.is_authorized(&(id.clone() + &token), true) {
                    Ok(true) => match gm.save(&id, &body, &revision) {
                        Ok((revision, payload)) => json(
                            &(JsPay {
                                revision,
                                token: None,
                                payload,
                            }),
                        ),
                        Err(_) => json(&server_error),
                    },
                    Ok(false) => json(&disallow),
                    Err(_) => json(&server_error),
                }
            },
        );

    let gm = memory.clone();
    let scanner =
        warp::path("scan")
            .and(warp::path::tail())
            .map(move |tail: warp::filters::path::Tail| {
                let id = Some(tail.as_str()).filter(|x| !x.is_empty());
                info!("Got scanning request for >>{}<< -> {:?}", tail.as_str(), id);

                let server_error = JsErr::new("server error".to_string());
                let empty = JsErr::new("empty".to_string());

                match gm.scan(id) {
                    Ok(None) => json(&(empty)),
                    Ok(Some((payload, id, next_id))) => json(
                        &(JsRecord {
                            id,
                            next_id,
                            payload,
                        }),
                    ),
                    Err(_) => json(&server_error),
                }
            });
    let gm = memory.clone();
    let secret_scanner = warp::path("secret_scan").and(warp::path::tail()).map(
        move |tail: warp::filters::path::Tail| {
            let id = Some(tail.as_str()).filter(|x| !x.is_empty());
            info!(
                "Got secret scanning request for >>{}<< -> {:?}",
                tail.as_str(),
                id
            );

            let server_error = JsErr::new("server error".to_string());
            let empty = JsErr::new("empty".to_string());

            match gm.secrets(id) {
                Ok(None) => json(&(empty)),
                Ok(Some((payload, id, next_id))) => json(
                    &(JsRecord {
                        id,
                        next_id,
                        payload,
                    }),
                ),
                Err(_) => json(&server_error),
            }
        },
    );
    let gm = memory.clone();
    let ownership_dropper = warp::path!("drop_ownership" / String).map(move |id: String| {
        let server_error = JsErr::new("server error".to_string());
        let was_not = JsErr::new("such id was not owned".to_string());
        let ok = JsOk::new("dropped".to_string());
        match gm.drop_ownership(&id) {
            Ok(true) => json(&ok),
            Ok(false) => json(&was_not),
            Err(_) => json(&server_error),
        }
    });
    let gm = memory.clone();
    let token_generator = warp::path!("issue_transfer_token").map(move || {
        let server_error = JsErr::new("server error".to_string());
        match gm.add_transfer_token() {
            Ok(x) => json(&(JsOk::new(x))),
            Err(_) => json(&server_error),
        }
    });

    let gm = memory.clone();
    let deauther = warp::path!("api" / String)
        .and(warp::delete())
        .and(warp::header::<String>("X-Token"))
        .map(move |id: String, token: String| {
            let server_error = JsErr::new("server error".to_string());
            let disallow = JsErr::new("not allowed".to_string());
            match gm.deauth(&(id + &token)) {
                Ok(()) => json(&disallow),
                Err(_) => json(&server_error),
            }
        });

    let dist = warp::path("dist").and(warp::fs::dir("./dist"));
    let index = warp::get().map(move || warp::reply::html(HTML_STUFF));

    let log = warp::log("traffic");
    let main_ops = getter
        .or(authenticator)
        .or(setter)
        .or(deauther)
        .or(dist)
        .or(index)
        .with(log);

    let main_server =
        warp::serve(main_ops).bind_with_graceful_shutdown(cfg.listen_address, shutdown_signal());

    let second_server = warp::serve(
        scanner
            .or(secret_scanner)
            .or(ownership_dropper)
            .or(token_generator)
            .with(log),
    )
    .bind_with_graceful_shutdown(([127, 0, 0, 1], cfg.admin_port), shutdown_signal());

    info!("Servers start now");
    join!(main_server.1, second_server.1);

    info!("Graceful server exit");
}
